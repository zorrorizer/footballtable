//POST REQUEST
// when document ready attach onclick events to buttons
$(document).ready(function () {

printDashboard();
 
  $('#add_game_btn').click(function (e) {
    e.preventDefault();
    if($('#teams_list1').val()==$('#teams_list2').val()){
      alert("Choose two different teams");
      return false;
    }

    //post with ajax to create new team
    $.ajax({
      url: './api/game/create.php',
      type: 'POST', // type of action POST || GET
      data: JSON.stringify({

        status: $('#teamname').val(),
        team1_id: $('#teams_list1').val(),
        team2_id: $('#teams_list2').val(),
        gf1: $('#gf1').val(),
        gf2: $('#gf2').val()

      }),
      dataType: 'json',

      success: function (feedback) {
        alert(feedback.message);
        printDashboard()//refresh dashboard
      },
      error: function () {
        alert('some error occured');
      },
    });
  });

  $('#add_team_btn').click(function (e) {
    e.preventDefault();

    //post with ajax to create new team
    $.ajax({
      url: './api/team/create.php',
      type: 'POST', // type of action POST || GET
      data: JSON.stringify({ name: $('#teamname').val() }),
      dataType: 'json',

      success: function (feedback) {
        alert(feedback.message);
        refreshTeamList(e); //refresh team list after adding new team
        printDashboard(); // refresh dashboard
      },
      error: function () {
        alert('some error occured');
      },
    });
  });

  // add on click event, without preventDefault() to refresh teams list on startup
  $('#refresh_team_btn').click(refreshTeamList());
});


// function fetching team list from API
function printDashboard() {
  var req;
  req = new XMLHttpRequest();
  req.open('GET', './api/dashboard/read.php', true);
  req.send();

  req.onload = function () {
    var son = JSON.parse(req.responseText);

    // clear dashboard
    $('#dashboard').html('');
 
    //append data into ul and selects
    for (i = 0; i < son.data.length; i++) {
      drawDashRow(son.data[i]);
    }
  };
}

function drawDashRow(row){

  
  let html = $('#dashboard').html();
  html += '<tr><td>' + row.name + ' </td>';
  html += '<td>' + row.gp + ' </td>';
  html += '<td>' + row.wins + ' </td>';
  html += '<td>' + row.loses + ' </td>';
  html += '<td>' + row.ratio + ' </td>';
  html += '<td>' + row.gf + ' </td>';
  html += '<td>' + row.ga + ' </td>';
  html += '<td>' + row.gd + ' </td></tr>';
  $('#dashboard').html(html);

}



// function fetching team list from API
function refreshTeamList() {
  var req;
  req = new XMLHttpRequest();
  req.open('GET', './api/team/read.php', true);
  req.send();

  req.onload = function () {
    var son = JSON.parse(req.responseText);

    // clear teams lists
    $('#teams_list').html('');
    $('#teams_list1').html('');
    $('#teams_list2').html('');

    if (son.data == undefined) {
      $('#teams_list').html('No teams in database');
      return false;
    }
    //append data into ul and selects

    for (i = 0; i < son.data.length; i++) {
      appendTeamIntoLists(son.data[i]);
    }
  };
}

function appendTeamIntoLists(team) {
  let html = $('#teams_list').html();
  html += '<li id="team' + team.id + '">' + team.name + ' </li>';
  $('#teams_list').html(html);

  // add delete team button
  var d = $('<input/>').attr({
    type: 'button',
    id: 'field',
    value: 'delete',
    onclick: 'deleteTeam(' + team.id + ')',
  });
  $('#team' + team.id).append(d);

  // put time list into options for select in new game form
  let optio = $('#teams_list1').html();
  optio += '<option value="' + team.id + '">' + team.name + '</option>';
  $('#teams_list1').html(optio);
  $('#teams_list2').html(optio);
}

function deleteTeam(id) {
  $.ajax({
    url: './api/team/delete.php?id=' + id,
    method: 'GET',
    success: function (response) {
      refreshTeamList();
      printDashboard();
    },
    error: function () {
      alert('some error occured');
    },
  });
}
