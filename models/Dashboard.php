<?php

class Dashboard
{
 // DB 
 private $conn;
 private $table = 'Dashboard';


 // Constructor with DB
 public function __construct($db)
 {
  $this->conn = $db;
 }

 // Get teams
 public function read()
 {
  // Create query
  $query = 'SELECT 
              teams.id,
              teams.name, 
              count(game_teams.id) as gp,
              sum(game_teams.gf) as gf, 
              sum(game_teams.ga) as ga, 
              sum(game_teams.win) as wins, 
              (count(game_teams.id)-sum(game_teams.win)) as loses,
              (sum(game_teams.win)/count(game_teams.id)) as ratio, 
              (sum(game_teams.gf)-sum(game_teams.ga)) as gd  
            FROM teams
            LEFT JOIN game_teams ON game_teams.team_id = teams.id
            GROUP BY game_teams.team_id
            ORDER BY ratio DESC, gd DESC';

  $stmt = $this->conn->prepare($query);
  // Execute query
  $stmt->execute();

  $row = $stmt->fetch(PDO::FETCH_ASSOC);

  // Execute query
  $stmt->execute();

  return $stmt;
 }





 //
}
