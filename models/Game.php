<?php

class Game
{
   // DB 
   private $conn;
   private $table    = 'games';
   private $table2   = 'game_teams';


   private $won   = '1';
   private $lost  = '0';

   // Properties
   public $gameid;
   public $status;

   public $team1_id;
   public $gf1;


   public $team2_id;
   public $gf2;


   public $created_at;




   // Constructor with DB
   public function __construct($db)
   {
      $this->conn = $db;
   }



   // Create Game
   public function create()
   {
      // Create Query
      $query = 'INSERT INTO ' .
         $this->table . '
    SET
      status = :status;';


      // Prepare Statement
      $stmt = $this->conn->prepare($query);

      // Clean data
      $this->status = htmlspecialchars(strip_tags($this->status));


      // Bind data
      $stmt->bindParam(':status', $this->status);

      // Execute query
      if ($stmt->execute()) {

         // if game was added successfully, now it's time to add data related to teams and their points with double insert query
         // get inserted game id
         $this->gameid = $this->conn->lastInsertId();

         // ad teams to game_teams
         $query = 'INSERT INTO ' . $this->table2 . '  SET
           game_id   = :gameid,
           team_id   = :team1_id,
           gf        = :gf1,
           ga        = :ga1,
           win       = :win1;';
         $query .= 'INSERT INTO ' . $this->table2 . ' SET
           game_id   = :gameid,
           team_id   = :team2_id,
           gf        = :gf2,
           ga        = :ga2,
           win       = :win2;';

         // Prepare Statement 2
         $stmt = $this->conn->prepare($query);

         // Bind data
         $stmt->bindParam(':gameid', $this->gameid);
         $stmt->bindParam(':team1_id', $this->team1_id);
         $stmt->bindParam(':team2_id', $this->team2_id);
         $stmt->bindParam(':gf1', $this->gf1);
         $stmt->bindParam(':gf2', $this->gf2);
         $stmt->bindParam(':ga1', $this->gf2);
         $stmt->bindParam(':ga2', $this->gf1);

         if ($this->gf1 == $this->gf2) {
            $stmt->bindParam(':win1', $this->lost);
            $stmt->bindParam(':win2', $this->lost);
         } else if ($this->gf1 > $this->gf2) {
            $stmt->bindParam(':win1', $this->won);
            $stmt->bindParam(':win2', $this->lost);
         } else {
            $stmt->bindParam(':win1', $this->lost);
            $stmt->bindParam(':win2', $this->won);
         }
         // execute double query
         if ($stmt->execute()) {
            return $stmt;
         }
      }

      printf(
         "Error : %s.\n",
         $stmt->error
      );

      return false;
   }
}
