/*
After I started designing the database I realizet 2 things
1. Firstly as I am antifan of football, and table football (I know it can disqalify me, but it's true) I have confused table football with football charts (there are tables too), when I realized mistake it was a little bit easier ;)
2. I need more data explaining the rules
    2.1 What are the team building rules, especilly if one player can be assigned to more than one team? Can he switch teams?
    2.2 Do we need to count points / goals for every player separately, or is it enough to count it for whole team?
   
Depending on the answer, and thus the degree of complication I might be tempted to create additional tables such as:

CREATE TABLE `players_dashboard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_id` int(11) NOT NULL,
  `wins` int(5) NOT NULL,
  `losses` int(5) NOT NULL,
  `gf` int(5) NOT NULL,
  `ga` int(5) NOT NULL,
  PRIMARY KEY (`id`)
);

And similar 
CREATE TABLE `teams_dashboard` ();
Which could be updated each time the game gets 'finnished' status

For a moment I was even thinking about adding

  `wins` int(5) NOT NULL,
  `losses` int(5) NOT NULL,
  `gf` int(5) NOT NULL,
  `ga` int(5) NOT NULL,

fields to  `teams` table or even to `players`, but I've decided that it can be counted on the run, based on the data from `game_teams`, so there's no need to duplicate entries...





*/



CREATE TABLE `teams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY unique_name (name)
);


CREATE TABLE `players` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team_id`  int(11) NOT NULL,  
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY unique_name (name)
);


CREATE TABLE `games` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(1) NOT NULL,  /*  might get 1 of 3 values e.g. 1 - game planned, 2 - game started, 3- game finnished   */
  /*
  depending on players requirements there might have been 3 additional fields
  `planned_at` datetime NOT NULL,
  `started_at` datetime NOT NULL,
  `finnished_at` datetime NOT NULL,
  */
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);

CREATE TABLE `game_teams` ( /*    for each `games` record there would be 2 entries in here `game_teams`, each for every playing team  */
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `game_id` int(5) NOT NULL,
  `team_id` int(5) NOT NULL,
  `gf` int(4) NULL,
  `ga` int(4) NULL,
  `win` int(1) NULL,
  PRIMARY KEY (`id`)
);


INSERT INTO `games` (`id`, `status`, `created_at`) VALUES
(1, 0, '2022-04-07 07:52:33'),
(2, 0, '2022-04-07 07:52:42'),
(3, 0, '2022-04-07 07:52:51'),
(4, 0, '2022-04-07 07:55:42'),
(5, 0, '2022-04-07 07:55:57'),
(6, 0, '2022-04-07 08:01:47'),
(7, 0, '2022-04-07 08:46:07'),
(8, 0, '2022-04-07 08:46:22'),
(9, 0, '2022-04-07 08:48:42'),
(10, 0, '2022-04-07 08:49:58'),
(11, 0, '2022-04-07 09:34:50'),
(12, 0, '2022-04-07 10:00:48'),
(13, 0, '2022-04-07 10:00:58'),
(14, 0, '2022-04-07 15:09:58'),
(15, 0, '2022-04-07 15:20:10'),
(16, 0, '2022-04-07 15:20:26');

INSERT INTO `game_teams` (`id`, `game_id`, `team_id`, `gf`, `ga`, `win`) VALUES
(1, 1, 14, 2, 4, 0),
(2, 1, 13, 4, 2, 1),
(3, 2, 13, 5, 7, 0),
(4, 2, 14, 7, 5, 1),
(5, 3, 11, 10, 2, 1),
(6, 3, 14, 2, 10, 0),
(7, 4, 14, 10, 4, 1),
(8, 4, 11, 4, 10, 0),
(9, 5, 14, 10, 3, 1),
(10, 5, 13, 3, 10, 0),
(11, 6, 13, 10, 2, 1),
(12, 6, 11, 2, 10, 0),
(13, 7, 13, 1, 7, 0),
(14, 7, 11, 7, 1, 1),
(15, 8, 14, 1, 7, 0),
(16, 8, 11, 7, 1, 1),
(17, 9, 16, 7, 1, 1),
(18, 9, 13, 1, 7, 0),
(19, 10, 17, 10, 4, 1),
(20, 10, 13, 4, 10, 0),
(21, 11, 18, 6, 7, 0),
(22, 11, 16, 7, 6, 1),
(23, 12, 19, 3, 1, 1),
(24, 12, 18, 1, 3, 0),
(25, 13, 19, 3, 6, 0),
(26, 13, 14, 6, 3, 1),
(27, 14, 21, 3, 6, 0),
(28, 14, 14, 6, 3, 1),
(29, 15, 23, 3, 5, 0),
(30, 15, 13, 5, 3, 1),
(31, 16, 23, 8, 5, 1),
(32, 16, 13, 5, 8, 0);

INSERT INTO `teams` (`id`, `name`, `created_at`) VALUES
(13, 'Bracia Bbbb', '2022-04-07 03:36:34'),
(14, 'Brave ones', '2022-04-07 04:16:37'),
(23, 'Team 5', '2022-04-07 15:19:59');
