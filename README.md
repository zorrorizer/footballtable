# PHP Test for Idibu by Maciej Balonis

# Table Footbal Heroes App

############# UPDATE 07.04.2022 21:28

I found a bug related to win/loose/draw. Draws are counted as looses. There should be separate field in game_teams
  `draw` int(1) NULL,
  It would allow to reconstruct query in order to fix the bug.

 ######################################

After I started designing the database I realizet 2 things
1. Firstly as I am antifan of football, and table football (I know it can disqalify me, but it's true) I have confused table football with football charts (there are tables too), when I realized mistake it was a little bit easier ;)
2. I need more data explaining the rules
    2.1 What are the team building rules, especilly if one player can be assigned to more than one team? Can he switch teams?
    2.2 Do we need to count points / goals for every player separately, or is it enough to count it for whole team?
   
Depending on the answer, and thus the degree of complication I might be tempted to create additional tables such as:

CREATE TABLE `players_dashboard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_id` int(11) NOT NULL,
  `wins` int(5) NOT NULL,
  `losses` int(5) NOT NULL,
  `gf` int(5) NOT NULL,
  `ga` int(5) NOT NULL,
  PRIMARY KEY (`id`)
);

And similar 
CREATE TABLE `teams_dashboard` ();
Which could be updated each time the game gets 'finnished' status

For a moment I was even thinking about adding

  `wins` int(5) NOT NULL,
  `losses` int(5) NOT NULL,
  `gf` int(5) NOT NULL,
  `ga` int(5) NOT NULL,

fields to  `teams` table or even to `players`, but I've decided that it can be counted on the run, based on the data from `game_teams`, so there's no need to duplicate entries...