<?php
// Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: DELETE');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization,X-Requested-With');

include_once '../../config/Database.php';
include_once '../../models/Team.php';
// Instantiate DB & connect
$database = new Database();
$db = $database->connect();

// Instantiate blog post object
$team = new Team($db);


// Get ID to delete
$team->id = $_GET['id'];

// Delete team
if ($team->delete()) {
  echo json_encode(
    array('message' => 'Team deleted')
  );
} else {
  echo json_encode(
    array('message' => 'Team not deleted')
  );
}
