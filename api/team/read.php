
<?php
// Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once '../../config/Database.php';
include_once '../../models/Team.php';

// Database instantiate and connect
$database = new Database();
$db = $database->connect();

// Instantiate team object
$team = new Team($db);

// Team read query
$result = $team->read();

// Get row count
$num = $result->rowCount();

// Check if any teams
if ($num > 0) {
 // Cat array
 $team_arr = array();
 $team_arr['data'] = array();

 while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
  extract($row);

  $team_item = array(
   'id' => $id,
   'name' => $name
  );

  // Push to "data"
  array_push($team_arr['data'], $team_item);
 }

 // Turn to JSON & output
 echo json_encode($team_arr);
} else {
 // No Teams
 echo json_encode(
  array('message' => 'No Teams Found')
 );
}
