<?php
// Headers
header('Access-Control-Allow-Origin: http://locaslhost');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization,X-Requested-With');

include_once '../../config/Database.php';
include_once '../../models/Team.php';

// Instantiate DB
$database = new Database();

//connect to database
$db = $database->connect();

// Team object instantate
$team = new Team($db);

// Getting json data
$data = json_decode(file_get_contents("php://input"));

//var_dump($data);

$team->name = $data->name;

// Create entry in teams table
if ($team->create()) {
 echo json_encode(
  array('message' => 'Team Created')
 );
} else {
 echo json_encode(
  array('message' => 'Team Not Created')
 );
}
