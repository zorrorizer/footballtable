
<?php
// Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once '../../config/Database.php';
include_once '../../models/Dashboard.php';

// Database instantiate and connect
$database = new Database();
$db = $database->connect();

// Instantiate dashboard object
$dashboard = new Dashboard($db);

// Dashboard read query
$result = $dashboard->read();

// Get row count
$num = $result->rowCount();

// Check if any data for dashboard
if ($num > 0) {
 // Cat array
 $dashboard_arr = array();
 $dashboard_arr['data'] = array();

 while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
  extract($row);

  $dashboard_items = array(
   'id'    => $id,
   'name'  => $name,
   'gp'    => $gp,
   'wins'  => $wins,
   'loses' => $loses,
   'ratio' => round($ratio, 2),
   'gf'    => $gf,
   'ga'    => $ga,
   'gd'    => $gd
  );

  // Push to "data"
  array_push($dashboard_arr['data'], $dashboard_items);
 }

 // Turn to JSON & output
 echo json_encode($dashboard_arr);
} else {
 // No Dashboards
 echo json_encode(
  array('message' => 'No Dashboards Found')
 );
}
