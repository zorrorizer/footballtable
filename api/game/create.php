<?php
// Headers
header('Access-Control-Allow-Origin: http://locaslhost');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization,X-Requested-With');

include_once '../../config/Database.php';
include_once '../../models/Game.php';

// Instantiate DB
$database = new Database();

//connect to database
$db = $database->connect();

// Game object instantate
$game = new Game($db);

// Getting json data
$data = json_decode(file_get_contents("php://input"));

//var_dump($data);

$game->status    = $data->status;
$game->team1_id  = $data->team1_id;
$game->gf1       = $data->gf1;
$game->team2_id  = $data->team2_id;
$game->gf2       = $data->gf2;



// Create entry in games table
if ($game->create()) {
 echo json_encode(
  array('message' => 'Game Created')
 );
} else {
 echo json_encode(
  array('message' => 'Game Not Created')
 );
}
